#! /bin/sh

col=`tput cols`

df -khl |
grep -E " (/Volumes|/System/Volumes/Data|/$)" |
awk -v col="$col" '{
  printf("\033[1;32m");
  for (i=9; i <= NF; i++) printf("%s ", $i);
  printf("\033[0m\n");
	
	for(c = 0; c < ($5 / 100) * (col / 4); c++) printf "█"; 
	printf "\033[1;30m"; for(c = 0; c < ((100 - $5) / 100) * (col / 4); c++) printf "▒"; 
	printf("\033[0m %s / %s (%s used)\n\n", $4, $2, $5);
}'
