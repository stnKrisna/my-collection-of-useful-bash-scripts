#! /bin/bash

if ! command -v brightness &> /dev/null
then
    echo 'Brightness: ???'
    exit
fi

col=`tput cols`

brightness -l | grep "display 0: brightness" | awk '{ print $NF }' |
awk -v col="$col" '{
    printf "Brightness\n";
    for(c = 0; c < $0 * (col / 4); c++) printf "█"; 
    printf "\033[1;30m"; for(c = 0; c < ((1 - $0)) * (col / 4); c++) printf "▒";
    printf("\033[0m (%d%%)", ($0 * 100));
}'

