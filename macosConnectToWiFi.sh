#! /bin/bash

networkStatus=$(ifconfig en0 | grep "status: " | cut -d\  -f2)

if [ $networkStatus == 'active'  ]
then
    echo "Already connected to a WiFi network"
else
    networksetup -setairportnetwork $1 $2 $3
fi

