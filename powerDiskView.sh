#! /bin/zsh

BASEDIR=$(dirname "$0")

cd $BASEDIR

while true; do

# Get string data
powerStatus=`./powerStats.sh`
storageStatus=`./diskSpace.sh`
screenBrightness=`./macbookScreenBrightness.sh`

# Display string
tput reset
printf "%s\n\n%s\n\n%s" $storageStatus $powerStatus $screenBrightness

# Wait 5 sec before restarting
isCharging=`system_profiler SPPowerDataType | grep -A3 -B7 "Condition" | grep "Charging" | awk '{print$2}'`

if $(test $isCharging = "Yes"); then
  sleep 1s
else
  sleep 5s
fi

done

