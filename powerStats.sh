#! /bin/sh

batteryLeft=`ioreg -l | awk '$3~/Capacity/{c[$3]=$5}END{OFMT="%.3f";max=c["\"MaxCapacity\""];print(max>0?100*c["\"CurrentCapacity\""]/max:"?")}'`

powerData=`system_profiler SPPowerDataType | grep -A3 -B7 "Condition"`
isCharging=`echo "$powerData" | grep "Charging" | awk '{print$2}'`
maxCharge=`echo "$powerData" | grep "Full Charge Capacity" | awk '{print$5}'`
isFullCharged=`echo "$powerData" | grep "Fully Charged" | awk '{print$3}'`

# Clear screen
# tput reset

# print stats
if $(test $isCharging = "No") && $(test $isFullCharged = "No"); then
	printf "Power (\e[1;31mDischarge\e[0m): $batteryLeft"\\n
elif $(test $isCharging = "No") && $(test $isFullCharged = "Yes"); then
	printf "Power (\e[1;32mCharged\e[0m): $batteryLeft"\\n
else
	printf "Power (\e[1;33mCharging\e[0m): $batteryLeft"\\n
fi

printf "Max charge (mAh): $maxCharge"\\n
	
