#! /bin/zsh

# -v = split horizontal
# -h = split veritical

BASEDIR=$(dirname "$0")

tmux new-session -d
tmux new-window -n "defaultWindow" -c $HOME

tmux set -g default-terminal "screen-256color"
tmux set -g pane-border-format " #{pane_title} "

# Bind key
tmux bind-key M-x kill-session

# Time
tmux clock-mode

# Terminal
tmux split-window -h -p 90 -t 1
tmux send-keys 'export TERM=xterm-256color;pwd' 'Enter'

# Show htop
tmux select-pane -t 0
tmux split-pane -v -p 90
tmux send-keys 'htop' 'Enter'

# Create ping view
tmux select-pane -t 2
tmux split-pane -v -p 15
tmux send-keys 'sleep 5s;gping 1.1.1.1' 'Enter'

# Neofetch
tmux select-pane -t 3
tmux split-pane -h
tmux send-keys "cd $BASEDIR" 'Enter'
tmux send-keys './powerDiskView.sh' 'Enter'

# Set pane title
tmux set -g pane-border-status top
tmux select-pane -t 0 -T Clock
tmux select-pane -t 1 -T Processes
tmux select-pane -t 2 -T Terminal
tmux select-pane -t 3 -T "Network Connectivity"
tmux select-pane -t 4 -T "Auxiliary Status"

# Move status bar on top
tmux set-option -g status-position top

# Activate command line
tmux select-pane -t 2

tmux attach-session -d

