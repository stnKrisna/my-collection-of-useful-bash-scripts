#! /bin/zsh

if [ $# -eq 1 ]; then
    cd "$1"
fi

python3 -m http.server
